import React, { Component } from "react";
import Geocode from "react-geocode";
import { geolocated } from "react-geolocated";
import { Map, InfoWindow, Marker, GoogleApiWrapper, SearchBox } from "google-maps-react";
import Paper from '@material-ui/core/Paper';
import { Grid } from "@material-ui/core";

Geocode.setApiKey("AIzaSyDARu9soBu4FsJIY9_-z7j5QtVeYkRpxFo");

class GoogleMap extends Component {
  state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: "",
    markerCoordinate: {
      lat: 0,
      lng: 0
    },
    address:"",
    city:"",
    area:"",
  };

  onMarkerClick = (props, marker, e) => {
    this.setState({
      activeMarker: marker,
      showingInfoWindow: true
    });
  };

  onMapClicked = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  onMapReady = () => {
    this.setState({
      markerCoordinate: {
        lat: this.props.coords.latitude,
        lng: this.props.coords.longitude
      }
    });

    this.getAddressfromCoordinates(
      this.state.markerCoordinate.lat,
      this.state.markerCoordinate.lng
    );
  };

  onMarkerDragEnd = coord => {
    const { latLng } = coord;
    const lat = latLng.lat();
    const lng = latLng.lng();
    console.log(lat,lng)
    this.setState({
      ...this.state,
      markerCoordinate: {
        lat: this.props.coords.latitude,
        lng: this.props.coords.longitude
      }
    });
    this.getAddressfromCoordinates(lat, lng);
  };

  getAddressfromCoordinates = (lat, lng) => {
    Geocode.fromLatLng(lat, lng).then(
      response => {
        const address = response.results[0].formatted_address;
        this.setState({ selectedPlace: address });
      },
      error => {
        console.error(error);
      }
    );
  };

  change = (center) => {
    console.log(center);
  };

  render() {
    let body = <h1>loading...</h1>;
    if (this.props.coords) {
      body = (
        <Map
          style={{
            height: "50%",
            width: "50%",
            margin: "0px auto"
          }}
          ref="map"
          google={this.props.google}
          zoom={14}
          onReady={this.onMapReady}
          initialCenter={{
            lat: this.props.coords.latitude,
            lng: this.props.coords.longitude
          }}
          onMouseover={(t,map,e)=>this.change(e)}
          onClick={this.onMapClicked}
        >
          <Marker
            draggable
            animation={this.props.google.maps.Animation.DROP}
            onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)}
            onClick={this.onMarkerClick}
          ></Marker>
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
            onClose={this.onMapClicked}
          >
            <div>
              <h1 style={{ fontSize: "10px" }}>{this.state.selectedPlace}</h1>
            </div>
          </InfoWindow>
        </Map>
      );
      
    }
    return (
      <div className="App">
        <Grid item xs style={{marginTop:"10%"}}>
          <Paper>
            <header className="App-header">{body}</header>
          </Paper>
        </Grid>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyDARu9soBu4FsJIY9_-z7j5QtVeYkRpxFo"
})(
  geolocated({
    positionOptions: {
      enableHighAccuracy: false
    },
    userDecisionTimeout: 1000
  })(GoogleMap)
);
